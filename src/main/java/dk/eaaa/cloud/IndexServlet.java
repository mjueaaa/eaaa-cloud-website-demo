package dk.eaaa.cloud;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dk.eaaa.cloud.model.User;


@WebServlet(urlPatterns= {"/index", "/", "/index.html"})
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public IndexServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		User user = (User)request.getSession().getAttribute("user");
		if(user == null) {
			response.sendRedirect("login");
			return;
		}
		
		request.getRequestDispatcher("/WEB-INF/Pages/index.jsp").forward(request, response);
	}

	
	

}
