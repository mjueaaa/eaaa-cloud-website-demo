package dk.eaaa.cloud;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dk.eaaa.cloud.model.User;


@WebServlet("/useredit")
public class UserEdit extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public UserEdit() {
        super();        
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User)request.getSession().getAttribute("user");
		if(user == null) {
			response.sendRedirect("login");
			return;
		}
		
		String userId = request.getParameter("userid");
		User editUser = new User("name for " + userId,"pwuser");
		
		request.setAttribute("editUser", editUser);		
        request.getRequestDispatcher("/WEB-INF/Pages/user_edit.jsp").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User)request.getSession().getAttribute("user");
		if(user == null) {
			response.sendRedirect("login");
			return;
		}
		
		String par1 = request.getParameter("par1");
        String par2 = request.getParameter("par2");
               
        response.getWriter().println("Edited user");
	}

}
