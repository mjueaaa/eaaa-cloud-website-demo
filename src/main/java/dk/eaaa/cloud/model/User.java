package dk.eaaa.cloud.model;

import java.io.Serializable;

public class User implements Serializable {
	private int    id;
	private String name;
	private String password;
	
	public User(String name, String passwd) {
		this.id = 0;
		this.name = name;
		this.password = passwd;
	}
	
	public int    getId() { return id; }
	public String getName() { return name; }
	
}
