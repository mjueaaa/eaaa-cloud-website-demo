package dk.eaaa.cloud;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dk.eaaa.cloud.model.User;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LoginServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/Pages/login.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userId   = request.getParameter("userId");
        String password = request.getParameter("password");
                 
        if("mj".equals(userId)) {                       
                         
            // request.getSession().setAttribute("username", userId);
            request.getSession().setAttribute("user", new User(userId,"pw"));
            // request.setAttribute("username", userId);
            // request.getRequestDispatcher("/hello").forward(request, response);
            response.sendRedirect("/index");
            return;
                         
        }
        
        response.sendRedirect("login");
	}

}
