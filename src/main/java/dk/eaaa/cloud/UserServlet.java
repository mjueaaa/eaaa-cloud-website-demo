package dk.eaaa.cloud;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dk.eaaa.cloud.model.User;


@WebServlet("/user")
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public UserServlet() {
        super();        
    }
	
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	User user = (User)request.getSession().getAttribute("user");
		if(user == null) {
			response.sendRedirect("login");
			return;
		}
		
		List<User> lst = new ArrayList<User>();
		lst.add(new User("user1","pw1"));
		lst.add(new User("user2","pw2"));
		lst.add(new User("user3","pw3"));
		
		request.setAttribute("userList", lst);		
        request.getRequestDispatcher("/WEB-INF/Pages/users.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User)request.getSession().getAttribute("user");
		if(user == null) {
			response.sendRedirect("login");
			return;
		}
		
		String par1 = request.getParameter("par1");
        String par2 = request.getParameter("par2");
               
        response.getWriter().println("Created user");        
        
	}


}
