<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="dk.eaaa.cloud.model.User"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

    <%    
    User editUser = (User)request.getAttribute("editUser");
    %>
    <h1>Edit User (<%= editUser.getName() %>)</h1>
    <form action='useredit' method='post'>
      <table>
      	<tr><td></td><td><input type='hidden' name='id' value='<%= editUser.getId() %>'/></td></tr>
      	<tr><td>Name1:</td><td><input type='text' name='name1' value='<%= editUser.getName() %>'/></td></tr>
      	<tr><td>Name2:</td><td><input type='text' name='name2' value='<%= editUser.getName() %>'/></td></tr>
      	<tr><td>Name3:</td><td><input type='text' name='name3' value='<%= editUser.getName() %>'/></td></tr>
      	<tr><td colspan='2'><input type='submit' value='Edit & Save '/></td></tr>
      </table>
    </form>
</body>
</html>