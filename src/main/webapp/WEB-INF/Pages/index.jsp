<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="dk.eaaa.cloud.model.User" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Index</title>
</head>
<body>

Main page!
<%
   User user = (User)request.getSession().getAttribute("user");
%>

<h1>Hello <%= user.getName() %></h1>

    <table>
      <tr>
        <td colspan="2" style="font-weight:bold;">Available Servlets:</td>        
      </tr>
      <tr>
        <td><a href='/hello'>The servlet</a></td>
      </tr>
      <tr>
        <td><a href='/login'>Login</a></td>
      </tr>
      <tr>
        <td><a href='/logout'>Logout</a></td>
      </tr>
      <tr>
        <td><a href='/index'>Index</a></td>
      </tr>
      <tr>
        <td><a href='/user'>Users</a></td>
      </tr>
      <tr>
        <td><a href='/useredit'>Edit user</a></td>
      </tr>
      
    </table>
    
    
</body>
</html>