<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.List,dk.eaaa.cloud.model.User"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<!-- <meta charset="utf-8"> -->
<title>User List</title>
	
</head>
<body>

<%! @SuppressWarnings("unchecked") %>
<%
List<User> userList = (List<User>)request.getAttribute("userList");
%>

userList size : <%= userList.size() %><br>

<h2>Users go here!</h2>

    
      <h2>User Listing (<a href="#">Create new User</a>)</h2>   
      <table class="table table-striped" >
	<thead>
	  <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Job</th>
            <th>Salary</th>
            <th>Details</th>
            <th>Edit</th>
            <th>Delete</th>        
	  </tr>
	</thead>
	<tbody>
	  <% for(int i=0; i<userList.size(); i++) { %>
             <% User u = userList.get(i); %>
             <tr>
               <td><%= u.getId()    %></td>
               <td><%= u.getName()  %></td>
               <td>job</td>
               <td>salary</td>
               <td>Details</td>
               <td><a href="/useredit?userid=<%= i %>">Edit</a></td>
               <td>Delete</td>
             </tr>        
	     <% } %>
	</tbody>
      </table>
      
<h2>Create a new user</h2>
<form action="user" method="post">
  User: <input type="text" name="userId" /><br>
  Password: <input type="password" name="password" /><br>
  <input type="submit" value="CREATE"/><br>
</form>
	
</body>
</html>